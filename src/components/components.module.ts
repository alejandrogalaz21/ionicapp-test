import { NgModule } from '@angular/core';
import { ProductsComponent } from './products/products';
@NgModule({
	declarations: [ProductsComponent],
	imports: [],
	exports: [ProductsComponent]
})
export class ComponentsModule {}
